$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("LinksHomePage.feature");
formatter.feature({
  "line": 2,
  "name": "Bracken Links and images",
  "description": "",
  "id": "bracken-links-and-images",
  "keyword": "Feature"
});
formatter.before({
  "duration": 48422579094,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Login page links verification",
  "description": "",
  "id": "bracken-links-and-images;login-page-links-verification",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@LinksCheck"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "the page has loaded",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I should verify all links in page",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 2719963384,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 179565611,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_HomePageLinks.i_should_verify_all_links_in_page()"
});
formatter.result({
  "duration": 3212521731,
  "status": "passed"
});
formatter.after({
  "duration": 464658068,
  "status": "passed"
});
formatter.uri("LoginPage.feature");
formatter.feature({
  "line": 2,
  "name": "Login",
  "description": "In order to use the app the user must be able to Login",
  "id": "login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@LogInFunctionality"
    }
  ]
});
formatter.before({
  "duration": 10384358712,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 5,
      "value": "#  you can give tag for every Scenario as wall like @Smock @Regression  and run When require"
    }
  ],
  "line": 6,
  "name": "001- Verify ‘Worlds best App’ page components",
  "description": "",
  "id": "login;001--verify-‘worlds-best-app’-page-components",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "the page has loaded",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I can see the page header has displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I can see a login title has displayed",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I can see a email  field has displayed",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I can see a password field has displayed",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I can see a login button has displayed",
  "keyword": "And "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 101224565,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 19113744,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_can_see_the_page_header_has_displayed()"
});
formatter.result({
  "duration": 971405372,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_can_see_a_login_title_has_displayed()"
});
formatter.result({
  "duration": 175658072,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_can_see_a_email_field_has_displayed()"
});
formatter.result({
  "duration": 114228510,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_can_see_a_password_field_has_displayed()"
});
formatter.result({
  "duration": 90588669,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_can_see_a_login_button_has_displayed()"
});
formatter.result({
  "duration": 82152663,
  "status": "passed"
});
formatter.after({
  "duration": 216757694,
  "status": "passed"
});
formatter.before({
  "duration": 14869043164,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "002- Verify the login functionality result with valid login credentials",
  "description": "",
  "id": "login;002--verify-the-login-functionality-result-with-valid-login-credentials",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 19,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "the page has loaded",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I enter valid email into email field",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "I enter valid password into password field",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "I should log in and  see welcome massage appears",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 31443867,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 21441135,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_enter_valid_email_into_email_field()"
});
formatter.result({
  "duration": 1198118034,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_enter_valid_password_into_password_field()"
});
formatter.result({
  "duration": 309694281,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_click_on_login_button()"
});
formatter.result({
  "duration": 444609973,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_should_log_in_and_see_welcome_massage_appears()"
});
formatter.result({
  "duration": 1334873612,
  "status": "passed"
});
formatter.after({
  "duration": 408642771,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 26,
  "name": "003- Verify the login functionality result with invalid login credentials",
  "description": "",
  "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 28,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "the page has loaded",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I enter invalid \"\u003cEmail\u003e\" into email field",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I enter invalid \"\u003cPassword\u003e\" into password field",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I should not login and see error massage appears",
  "keyword": "Then "
});
formatter.examples({
  "comments": [
    {
      "line": 35,
      "value": "#     this will also  check with one of field blank and one of filed with valid credentials"
    }
  ],
  "line": 37,
  "name": "",
  "description": "",
  "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;",
  "rows": [
    {
      "cells": [
        "Email",
        "Password"
      ],
      "line": 38,
      "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;1"
    },
    {
      "cells": [
        "test@drugdev.com",
        ""
      ],
      "line": 39,
      "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;2"
    },
    {
      "cells": [
        "",
        "supers3cret"
      ],
      "line": 40,
      "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;3"
    },
    {
      "cells": [
        "",
        ""
      ],
      "line": 41,
      "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;4"
    },
    {
      "cells": [
        "te123@drugdev.com",
        "supers3cret"
      ],
      "line": 42,
      "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;5"
    },
    {
      "cells": [
        "test@drugdev.com",
        "fine3cret"
      ],
      "line": 43,
      "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;6"
    },
    {
      "cells": [
        "automation@gmail.com",
        "selenium"
      ],
      "line": 44,
      "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;7"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 15866376295,
  "status": "passed"
});
formatter.scenario({
  "line": 39,
  "name": "003- Verify the login functionality result with invalid login credentials",
  "description": "",
  "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@LogInFunctionality"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "the page has loaded",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I enter invalid \"test@drugdev.com\" into email field",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I enter invalid \"\" into password field",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I should not login and see error massage appears",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 32009926,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 21773112,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test@drugdev.com",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_email_field(String)"
});
formatter.result({
  "duration": 586571787,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_password_field(String)"
});
formatter.result({
  "duration": 359583596,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_click_on_login_button()"
});
formatter.result({
  "duration": 134183052,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_should_not_login_and_see_error_massage_appears()"
});
formatter.result({
  "duration": 968285341,
  "status": "passed"
});
formatter.after({
  "duration": 4356246852,
  "status": "passed"
});
formatter.before({
  "duration": 20172191458,
  "status": "passed"
});
formatter.scenario({
  "line": 40,
  "name": "003- Verify the login functionality result with invalid login credentials",
  "description": "",
  "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@LogInFunctionality"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "the page has loaded",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I enter invalid \"\" into email field",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I enter invalid \"supers3cret\" into password field",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I should not login and see error massage appears",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 88265225,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 23457471,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_email_field(String)"
});
formatter.result({
  "duration": 464040693,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "supers3cret",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_password_field(String)"
});
formatter.result({
  "duration": 473836580,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_click_on_login_button()"
});
formatter.result({
  "duration": 349281651,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_should_not_login_and_see_error_massage_appears()"
});
formatter.result({
  "duration": 781692159,
  "status": "passed"
});
formatter.after({
  "duration": 638988232,
  "status": "passed"
});
formatter.before({
  "duration": 18337268110,
  "status": "passed"
});
formatter.scenario({
  "line": 41,
  "name": "003- Verify the login functionality result with invalid login credentials",
  "description": "",
  "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@LogInFunctionality"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "the page has loaded",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I enter invalid \"\" into email field",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I enter invalid \"\" into password field",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I should not login and see error massage appears",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 28246466,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 19637565,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_email_field(String)"
});
formatter.result({
  "duration": 223787633,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_password_field(String)"
});
formatter.result({
  "duration": 287436032,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_click_on_login_button()"
});
formatter.result({
  "duration": 196636572,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_should_not_login_and_see_error_massage_appears()"
});
formatter.result({
  "duration": 814004851,
  "status": "passed"
});
formatter.after({
  "duration": 140797724,
  "status": "passed"
});
formatter.before({
  "duration": 12418888268,
  "status": "passed"
});
formatter.scenario({
  "line": 42,
  "name": "003- Verify the login functionality result with invalid login credentials",
  "description": "",
  "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@LogInFunctionality"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "the page has loaded",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I enter invalid \"te123@drugdev.com\" into email field",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I enter invalid \"supers3cret\" into password field",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I should not login and see error massage appears",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 58106239,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 16832142,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "te123@drugdev.com",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_email_field(String)"
});
formatter.result({
  "duration": 2481497315,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "supers3cret",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_password_field(String)"
});
formatter.result({
  "duration": 267096618,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_click_on_login_button()"
});
formatter.result({
  "duration": 108985959,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_should_not_login_and_see_error_massage_appears()"
});
formatter.result({
  "duration": 1302442893,
  "status": "passed"
});
formatter.after({
  "duration": 367853810,
  "status": "passed"
});
formatter.before({
  "duration": 8075524176,
  "status": "passed"
});
formatter.scenario({
  "line": 43,
  "name": "003- Verify the login functionality result with invalid login credentials",
  "description": "",
  "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@LogInFunctionality"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "the page has loaded",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I enter invalid \"test@drugdev.com\" into email field",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I enter invalid \"fine3cret\" into password field",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I should not login and see error massage appears",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 52033547,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 32699143,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test@drugdev.com",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_email_field(String)"
});
formatter.result({
  "duration": 245250479,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "fine3cret",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_password_field(String)"
});
formatter.result({
  "duration": 173132915,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_click_on_login_button()"
});
formatter.result({
  "duration": 252264233,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_should_not_login_and_see_error_massage_appears()"
});
formatter.result({
  "duration": 787035370,
  "status": "passed"
});
formatter.after({
  "duration": 80340014,
  "status": "passed"
});
formatter.before({
  "duration": 11117459464,
  "status": "passed"
});
formatter.scenario({
  "line": 44,
  "name": "003- Verify the login functionality result with invalid login credentials",
  "description": "",
  "id": "login;003--verify-the-login-functionality-result-with-invalid-login-credentials;;7",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@LogInFunctionality"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "I have navigated to login page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "the page has loaded",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I enter invalid \"automation@gmail.com\" into email field",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I enter invalid \"selenium\" into password field",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I should not login and see error massage appears",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef_LogIn.i_have_navigated_to_login_page()"
});
formatter.result({
  "duration": 35197852,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.the_page_has_loaded()"
});
formatter.result({
  "duration": 13957640,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "automation@gmail.com",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_email_field(String)"
});
formatter.result({
  "duration": 331955688,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "selenium",
      "offset": 17
    }
  ],
  "location": "StepDef_LogIn.i_enter_invalid_into_password_field(String)"
});
formatter.result({
  "duration": 822954414,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_click_on_login_button()"
});
formatter.result({
  "duration": 271764823,
  "status": "passed"
});
formatter.match({
  "location": "StepDef_LogIn.i_should_not_login_and_see_error_massage_appears()"
});
formatter.result({
  "duration": 293265170,
  "status": "passed"
});
formatter.after({
  "duration": 727217141,
  "status": "passed"
});
});