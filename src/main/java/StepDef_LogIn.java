import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class StepDef_LogIn extends Driver
{

    @Given("^I have navigated to login page$")
    public void i_have_navigated_to_login_page()
    {
     // @Before annotation method will navigate to login page there
        Assert.assertTrue(driver.getCurrentUrl().contains("sprinkle-burn.glitch.me"));
        Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Worlds Best App"));

    }

    @When("^the page has loaded$")
    public void the_page_has_loaded()
    { // page lord method call from utility class
       Utility.PageLord(4);
    }

    @Then("^I can see the page header has displayed$")
    public void i_can_see_the_page_header_has_displayed()
    { // Header location coming from Locators_loginPage class
     Assert.assertTrue(Locators_LoginPage.header().isDisplayed());

    }
    @Then("^I can see a login title has displayed$")
    public void i_can_see_a_login_title_has_displayed() throws Throwable
    {
        Assert.assertTrue(Locators_LoginPage.loginHeader().isDisplayed());
    }

    @Then("^I can see a email  field has displayed$")
    public void i_can_see_a_email_field_has_displayed()
    {
        Assert.assertTrue(Locators_LoginPage.emailFild().isDisplayed());
    }

    @Then("^I can see a password field has displayed$")
    public void i_can_see_a_password_field_has_displayed()
    {
        Assert.assertTrue(Locators_LoginPage.passwordFild().isDisplayed());
    }


    @Then("^I can see a login button has displayed$")
    public void i_can_see_a_login_button_has_displayed()
    {
        Assert.assertTrue(Locators_LoginPage.logInButton().isDisplayed());
    }


    @When("^I enter valid email into email field$")
    public void i_enter_valid_email_into_email_field()
    {
       Utility.enterText(Locators_LoginPage.emailFild(),"test@drugdev.com");
    }

    @When("^I enter valid password into password field$")
    public void i_enter_valid_password_into_password_field()
    {
        Utility.enterText(Locators_LoginPage.passwordFild(),"supers3cret");

    }

    @When("^I click on login button$")
    public void i_click_on_login_button()
    {
      Utility.clickOnElement(Locators_LoginPage.logInButton());

    }

    @Then("^I should log in and  see welcome massage appears$")
    public void i_should_log_in_and_see_welcome_massage_appears ()
    {

        try {
            // Explicitly Wait given to give some more time to find next element
              Utility.WaitExplicitly(Locators_LoginPage.succsessfullLoginMassage(),10);

            // calling get_Text method from Utility class and locators from Locators_LoginPage
            Assert.assertEquals(Utility.get_Text(Locators_LoginPage.succsessfullLoginMassage()),"Welcome Dr I Test");
        }
        catch(Exception e)
        {
            Assert.assertTrue(e.getMessage().contains("no such element"));
            e.printStackTrace();
        }

        }

        @When("^I enter invalid \"([^\"]*)\" into email field$")
        public void i_enter_invalid_into_email_field (String Email)
        {
            // this Email arg data coming from Example of scenario Outlet
         Utility.enterText(Locators_LoginPage.emailFild(),Email);
        }


        @When("^I enter invalid \"([^\"]*)\" into password field$")
        public void i_enter_invalid_into_password_field (String Password)
        {
            // this Password arg data coming from Example of scenario Outlet
        Utility.enterText(Locators_LoginPage.passwordFild(),Password);
        }

        @Then("^I should not login and see error massage appears$")
            public void i_should_not_login_and_see_error_massage_appears ()
        {

            try {
                // Explicitly Wait given to give some more time to find next element
                Utility.WaitExplicitly(Locators_LoginPage.errorMassage(),10);
                Assert.assertEquals(Utility.get_Text(Locators_LoginPage.errorMassage()),"Credentials are incorrect");
            }
            catch(Exception e)
            {
                Assert.assertTrue(e.getMessage().contains("no such element"));
                e.printStackTrace();
            }

        }


}