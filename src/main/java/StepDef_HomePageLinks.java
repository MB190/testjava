import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class StepDef_HomePageLinks extends Driver {


    @Then("^I should verify all links in page$")
    public void i_should_verify_all_links_in_page() {


        List<WebElement> links = driver.findElements(By.tagName("a"));

        System.out.println("Total links are " + links.size());

        for (int i = 0; i < links.size(); i++) {

            WebElement ele = links.get(i);

            String url = ele.getAttribute("href");

            Utility.verifyLinkActive(url);
            System.out.println(url);

        }
    }
}