import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Locators_LoginPage extends Driver
{
    // This class has all home page locators
    //  store in methods & this method calls by StepDef

    public static WebElement header ()
    { WebElement element=driver.findElement(By.xpath("//h1[contains(text(),'Worlds Best App')]"));
        return element;
    }

    public static WebElement loginHeader ()
    { WebElement element=driver.findElement(By.xpath("//h1[@class='ma0 mb2']"));
        return element;
    }

    public static WebElement emailFild ()
    { WebElement element=driver.findElement(By.name("email"));
        return element;
    }

    public static WebElement passwordFild ()
    { WebElement element=driver.findElement(By.name("password"));
        return element;
    }

    public static WebElement logInButton ()
    { WebElement element=driver.findElement(By.xpath("//button[@class='f5 dim bn ph3 pv2 mb2 dib br1 white bg-dark-green']"));

        return element;
    }

    public static WebElement succsessfullLoginMassage ()
    { WebElement element=driver.findElement(By.xpath("//article[@class='pa4 black-80 content']"));

        return element;
    }

    public static WebElement errorMassage ()
    { WebElement element=driver.findElement(By.id("login-error-box"));

        return element;
    }


}
